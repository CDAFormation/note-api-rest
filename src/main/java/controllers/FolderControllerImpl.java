package controllers;

import model.Folder;

import dao.FolderDAO;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;


import java.util.List;
import java.util.Optional;
import java.util.UUID;

@ApplicationScoped
public class FolderControllerImpl implements FolderController {
    @Inject
    private FolderDAO folderDAO;

    @Override
    public List<Folder> getAll() {
        return folderDAO.getAll();
    }

    @Override
    public Folder getById(UUID folder_id) {
//        var maybeFolder = folderDAO.getById(folder_id);
//        return Optional.ofNullable(maybeFolder);
        return folderDAO.getById(folder_id);
    }

    @Override
    public Optional<Folder> getByName(String name) {
        var maybeFolder = folderDAO.getByName(name);
        return Optional.ofNullable(maybeFolder);
    }

    @Override
    @Transactional
    public Folder createFolder(String name) {
        var folder = new Folder(name);
        folderDAO.persist(folder);
        return folder;
    }

    @Override
    @Transactional
    public void delete(Folder folder) {
        folderDAO.delete(folder);
    }

    @Override
    @Transactional
    public Folder update(Folder folder, String name) {
        folderDAO.update(folder, name);
        return folder;
    }
}
