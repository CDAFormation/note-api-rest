package controllers;

import model.User;

import java.util.List;
import java.util.Optional;

public interface UserController {
    User createUser(String login);
    List<User> getAll();
    Optional<User> getByLogin(String login);
    void delete(User user);
    User updateLogin(User user, String login) throws ExistentLoginException;
}
