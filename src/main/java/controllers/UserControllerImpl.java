package controllers;

import dao.UserDAO;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import model.User;

import java.util.List;
import java.util.Optional;

//@ApplicationScoped
public class UserControllerImpl implements UserController {
    @Inject
    private UserDAO userDAO;

    @Override
    public User createUser(String login) {
        var user = new User(login);
        userDAO.persist(user);
        return user;
    }

    @Override
    public List<User> getAll() {
        return userDAO.getAll();
    }

    @Override
    public Optional<User> getByLogin(String login) {
        var maybeUser = userDAO.getByLogin(login);
        return Optional.ofNullable(maybeUser);
    }

    @Override
    public void delete(User user) {
        userDAO.delete(user);
    }

    @Override
    public User updateLogin(User user, String login) throws ExistentLoginException {
        if (userDAO.getByLogin(login) != null) {
            throw new ExistentLoginException();
        }

        user.setLogin(login);
        userDAO.update(user);

        return null;
    }
}
