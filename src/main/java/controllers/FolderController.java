package controllers;

import model.Folder;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface FolderController {
    Folder createFolder(String name);
    List<Folder> getAll();
    Optional<Folder> getByName(String name);
    Folder getById(UUID folder_id);
    void delete(Folder folder);

    Folder update(Folder folder, String name);
}
