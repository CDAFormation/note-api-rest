package controllers;

import model.Note;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface NoteController {
    List<Note> getAll();
    Note getById(UUID note_id);
    Optional<Note> getByTitle(String title);
    List<Note> getByFolderId(UUID folder_id);
    Note createNote(String title, UUID folder_id);
    void delete(Note note);
    Note update(UUID note_id, String title);
//    delete(UUID note_id);
}
