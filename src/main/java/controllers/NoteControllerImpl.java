package controllers;

import model.Folder;
import model.Note;

import dao.NoteDAO;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@ApplicationScoped
public class NoteControllerImpl implements NoteController {
    @Inject
    private NoteDAO noteDAO;
    @Inject
    private FolderController folderController;

    @Override
    public List<Note> getAll() {
        return noteDAO.getAll();
    }

    @Override
    public Note getById(UUID note_id) {
        return noteDAO.getById(note_id);
    }

    @Override
    public Optional<Note> getByTitle(String title) {
        var maybeNote = noteDAO.getByTitle(title);
        return Optional.ofNullable(maybeNote);
    }

    @Override
    public List<Note> getByFolderId(UUID folder_id) {
//        var maybeNote = noteDAO.getByFolderId(folder_id);
//        return Optional.ofNullable(maybeNote);
        Folder folder = folderController.getById(folder_id);
        return noteDAO.getByFolderId(folder);
    }

    @Override
    @Transactional
    public Note createNote(String title, UUID folder_id) {
        Folder folder = folderController.getById(folder_id);
        Note note = new Note(title, folder);
        noteDAO.persist(note);
        return note;
    }

    @Override
    @Transactional
    public Note update(UUID note_id, String title) {
//        Note note = getById(note_id);
        return noteDAO.update(getById(note_id), title);
    }

    @Override
    @Transactional
    public void delete(Note note) {
        noteDAO.delete(note);
    }
}
