package dao;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import model.Folder;
import model.Note;

import java.util.List;
import java.util.UUID;

@ApplicationScoped
public class NoteJpaDAO implements NoteDAO {
    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Note> getAll() {
        var jpql = "SELECT n FROM Note n";
        return em.createQuery(jpql, Note.class)
                .getResultList();
    }

    @Override
    public Note getByTitle(String title) {
        var jpql = "select n from Note n where n.title = :title";
        return em.createQuery(jpql, Note.class)
                .setParameter("title", title)
                .getSingleResult();
    }

    @Override
    public Note getById(UUID note_id) {
        var jpql = "select n from Note n where n.id = :note_id";
        return em.createQuery(jpql, Note.class)
                .setParameter("note_id", note_id)
                .getSingleResult();
    }

    @Override
    public List<Note> getByFolderId(Folder folder) {
        var jpql = "select n from Note n where n.folders = :folder_id";
        return em.createQuery(jpql, Note.class)
                .setParameter("folder_id", folder)
                .getResultList();
    }

    @Override
    public void persist(Note note) {
        em.persist(note);
    }

    @Override
    public Note update(Note note, String title) {
        note.setTitle(title);
        return em.merge(note);
    }

    @Override
    public void delete(Note note) {
//        var note = em.merge(note);
        em.remove(em.merge(note));
    }
}
