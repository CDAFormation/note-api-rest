package dao;

import jakarta.enterprise.context.ApplicationScoped;
import model.User;

import java.util.List;

public class UserInMemoryDAO {
    private static List<User> users = List.of(
            new User("Rincevent"),
            new User("Veterini"),
            new User("Vimaire")
    );

    public List<User> getAll() {
        return List.copyOf(users);
    }

    public User getByLogin(String login) {
        User result = null;

        for(var user : users) {
            if(user.getLogin().equals(login)) {
                result = user;
            }
        }

        return result;
    }
}
