package dao;

import model.Folder;
import model.Note;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface FolderDAO {
    List<Folder> getAll();

    Folder getByName(String name);
    Folder getById(UUID folder_id);
    void persist(Folder folder);
    void delete(Folder folder);
    void update(Folder folder, String name);
}
