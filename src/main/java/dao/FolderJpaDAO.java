package dao;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import model.Folder;
import model.Note;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@ApplicationScoped
public class FolderJpaDAO implements FolderDAO {
    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Folder> getAll() {
        var jpql = "SELECT f FROM Folder f";
        return em.createQuery(jpql, Folder.class)
                .getResultList();
    }

    @Override
    public Folder getByName(String name) {

        var jpql ="select f from  Folder f where f.name = :name";
        return em.createQuery(jpql,Folder.class)
                .setParameter("name", name)
                .getSingleResult();
    }

    @Override
    public Folder getById(UUID folder_id) {
        var jpql ="select f from Folder f where f.id = :folder_id";
        return em.createQuery(jpql,Folder.class)
                .setParameter("folder_id", folder_id)
                .getSingleResult();
    }

    @Override
    public void persist(Folder folder) {
        em.persist(folder);
    }

    @Override
    public void delete(Folder folder) {
        var fold = em.merge(folder);
        em.remove(fold);
//        em.remove(em.merge(folder));
    }

    @Override
    public void update(Folder folder, String name) {
        folder.setName(name);
        em.merge(folder);
    }

//    @Override
//    public User getByLogin(String login) {
//        return null;
//    }
//
//    @Override
//    public void persist(User user) {
//
//    }
//
//    @Override
//    public void delete(User user) {
//
//    }
//
//    @Override
//    public void update(User user) {

//    }
}
