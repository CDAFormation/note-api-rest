package dao;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import model.User;

import javax.swing.text.html.parser.Entity;
import java.util.List;

@ApplicationScoped
public class UserJpaDAO implements UserDAO {
    @PersistenceContext
    private EntityManager em;

    @Override
    public List<User> getAll() {
        var jpql = "SELECT u FROM User u";
        return em.createQuery(jpql)
                .getResultList();
    }

    @Override
    public User getByLogin(String login) {
        return null;
    }

    @Override
    public void persist(User user) {

    }

    @Override
    public void delete(User user) {

    }

    @Override
    public void update(User user) {
    }
}
