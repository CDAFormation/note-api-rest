package dao;

import model.Folder;
import model.Note;

import java.util.List;
import java.util.UUID;

public interface NoteDAO {
    List<Note> getAll();
    Note getByTitle(String title);
    Note getById(UUID note_id);
    List<Note> getByFolderId(Folder folder);
    void persist(Note note);
    void delete(Note note);
    Note update(Note note, String title);
}
