package model;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import java.util.UUID;

@Entity
@Table(name = "users")
public class User {
    @Id
    private UUID id;
    @Column(name = "login")
    private String login;

    public User(String login) {
        this.id = UUID.randomUUID();
        this.login = login;
    }

    protected User() {

    }

    public UUID getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    private User(UUID id, String login) {
        this.id = id;
        this.login = login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public static User hydrate (UUID id, String login) {
        return new User(id, login);
    }
}
