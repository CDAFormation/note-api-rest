package model;

import jakarta.persistence.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "notes")
public class Note {
    @Id
    @GeneratedValue()
    private UUID id;
    @Column(name = "title")
    private String title;
    @Column(name = "datetime")
    private Timestamp datetime;
    @Column(name = "content")
    private String content;

    @ManyToOne
    private Folder folders;

    public Note() {

    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Timestamp getDatetime() {
        return datetime;
    }

    public void setDatetime(Timestamp datetime) {
        this.datetime = datetime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public UUID getId() {
        return id;
    }

    public Folder getFolders() {
        return folders;
    }

    public void setFolders(Folder folders) {
        this.folders = folders;
    }

    public Note(String title, Folder folder) {
        this.title = title;
        this.content = "du contenu";
        Date d = new Date();
        this.datetime = new Timestamp(d.getTime());
        this.folders = folder;
    }
}
