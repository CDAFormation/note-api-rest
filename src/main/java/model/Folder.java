package model;

import jakarta.persistence.*;

import java.util.UUID;


@Entity
@Table(name = "folders")
public class Folder {

    @Id
    @GeneratedValue
    private UUID id;
    @Column(name = "name")
    private String name;


    public Folder(String name) {
        this.name = name;
    }

    private Folder(UUID id, String login) {
        this.id = id;
        this.name = login;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Folder() {

    }

    public String getName() {
        return name;
    }

    public UUID getId() {
        return id;
    }

    public static Folder hydrate (UUID id, String name) {
        return new Folder(id,name);
    }

}
