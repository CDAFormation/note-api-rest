package api;

import controllers.NoteController;
import model.Folder;

import controllers.FolderController;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;
import model.Note;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;

@ApplicationScoped
@Path("/folders")
public class FolderResource {
    @Inject
    private FolderController folderController;

    @Inject
    private NoteController noteController;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Folder> getAll() {

        return this.folderController.getAll();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Folder createFolder(@Context UriInfo uriInfo, FolderJsonInput json) throws URISyntaxException {
        var folder = folderController.createFolder(json.name);
//        var uri = uriInfo.getPath() + folder.getName();

//        return Response
//                .created(new URI(uri))
//                .entity(folder)
//                .build();
        return folder;
    }

    @GET
    @Path("/{folder_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Folder getFolder(@PathParam("folder_id") UUID folder_id) {
//        return folderController
//                .getById(folder_id)
//                .orElseThrow(NotFoundException::new);

        return folderController
                .getById(folder_id);
    }


    @Path("/{folder_id}")
    @DELETE
    public void deleteFolder(@PathParam("folder_id") UUID folder_id) {
        var folder = folderController
//                .getById(folder_id)
//                .orElseThrow(NotFoundException::new);

                .getById(folder_id);

        folderController.delete(folder);
    }

    @Path("/{folder_id}")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
//    @Context UriInfo uriInfo, FolderJsonInput json) throws URISyntaxException {
//    public void updateFolder(@Context UriInfo uriInfo, FolderJsonInput json) throws URISyntaxException {
    public Folder updateFolder(@PathParam("folder_id") UUID folder_id, FolderJsonInput json) throws URISyntaxException {
        var folder = folderController
//                .getById(folder_id)
//                .orElseThrow(NotFoundException::new);

                .getById(folder_id);

        folderController.update(folder, json.name);
        return folder;
    }


    // ROUTES NOTES
    @Path("{folderid}/notes")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Note createNote(@Context UriInfo uriInfo, NoteJsonInput json) throws URISyntaxException {
        var note = noteController.createNote(json.title, json.folder_id);
//        var uri = uriInfo.getPath() + note.getTitle();


        return note;
//        return Response
//                .created(new URI(uri))
//                .entity(note)
//                .build();
        /** TODO
         * Return location instead of Note note
         */
    }

    @Path("{folder_id}/notes/{note_id}")
    @DELETE
    public void deleteNote(@PathParam("note_id") UUID note_id) {
        var note = noteController
//                .getById(note_id)
//                .orElseThrow(NotFoundException::new);

                .getById(note_id);
        noteController.delete(note);
    }

    @Path("{folder_id}/notes/{note_id}")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
//    @Context UriInfo uriInfo, FolderJsonInput json) throws URISyntaxException {
//    public void updateFolder(@Context UriInfo uriInfo, FolderJsonInput json) throws URISyntaxException {
    public Note updateNote(@PathParam("note_id") UUID note_id, NoteJsonInput json) throws URISyntaxException {
        return noteController.update(note_id, json.title);

    }

    @Path("{folder_id}/notes")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Note> getNotesByFolderId(@PathParam("folder_id") UUID folder_id) {
        return noteController
                .getByFolderId(folder_id);
    }

    @Path("{folder_id}/notes/{note_id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Note getNote(@PathParam("note_id") UUID note_id) {
//        var note = noteController
//                .getById(note_id)
//                .orElseThrow(NotFoundException::new);

//                .getById(note_id);
        return noteController.getById(note_id);
    }
}
