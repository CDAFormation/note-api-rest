//package api;
//
//import controllers.ExistentLoginException;
//import controllers.UserController;
//import jakarta.enterprise.context.ApplicationScoped;
//import jakarta.inject.Inject;
//import jakarta.ws.rs.*;
//import jakarta.ws.rs.core.Context;
//import jakarta.ws.rs.core.MediaType;
//import jakarta.ws.rs.core.Response;
//import jakarta.ws.rs.core.UriInfo;
//import model.User;
//
//import java.net.URI;
//import java.net.URISyntaxException;
//import java.util.List;
//
////@ApplicationScoped
//@Path("/users")
//public class UserResource {
//    @Inject
//    private UserController userController;
//
//    @GET
//    @Produces(MediaType.APPLICATION_JSON)
//    public List<User> getAllUsers() {
//        return this.userController.getAll();
//    }
//
//    @POST
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Produces(MediaType.APPLICATION_JSON)
//    public Response createUser(@Context UriInfo uriInfo, UserJsonInput json) throws URISyntaxException {
//        var user = userController.createUser(json.login);
//        var uri = uriInfo.getPath() + user.getLogin();
//
//        return Response
//                .created(new URI(uri))
//                .entity(user)
//                .build();
//    }
//
//    @Path("/{login}")
//    @DELETE
//    public void deleteUser(@PathParam("login") String login) {
//        var user = userController
//                .getByLogin(login)
//                .orElseThrow(NotFoundException::new);
//
//        userController.delete(user);
//    }
//
//    @Path("/{login}")
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Produces(MediaType.APPLICATION_JSON)
//    @PUT
//    public User updateUser(@PathParam("login") String login, UserJsonInput json) {
//        var user = userController
//                .getByLogin(login)
//                .orElseThrow(NotFoundException::new);
//
//        try {
//            return userController.updateLogin(user, json.login);
//        } catch (ExistentLoginException e) {
//            throw new BadRequestException();
//        }
//    }
//
//    @Path("/{login}")
//    @GET
//    @Produces(MediaType.APPLICATION_JSON)
//    public User getUser(@PathParam("login") String login) {
//        return userController
//                .getByLogin(login)
//                .orElseThrow(NotFoundException::new);
//    }
//
////    @GET
////    @Produces(MediaType.APPLICATION_JSON)
////    public List<User> getAllUsers() {
////        return this.userController.getAll();
////    }
//}
