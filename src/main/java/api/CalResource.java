package api;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;

@Path("/cal")
public class CalResource {
@GET
    public String cal(
            @QueryParam("a") Integer a,
            @QueryParam("operator") String operator,
            @QueryParam("b") Integer b) {
    if (operator == null || a == null || b == null){
        return "Error";
    }
        var result = switch (operator) {
            case "plus" -> String.valueOf(a+b);
            case "less" -> String.valueOf(a-b);
            case "dividedby" -> String.valueOf(a/b);
            case "times" -> String.valueOf(a*b);
            default -> "error";
        };
                return result;
    }
}
