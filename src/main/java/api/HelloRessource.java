package api;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

@Path("/hello")
public class HelloRessource {
    @GET
    public String sayHello() {
        return "Hello, World!";
    }
}
