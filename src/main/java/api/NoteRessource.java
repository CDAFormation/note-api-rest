package api;

import model.Note;

import controllers.NoteController;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.UriInfo;

import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;

@ApplicationScoped
@Path("/")
public class NoteRessource {
    @Inject
    private NoteController noteController;






    @GET
    @Path("/{title}")
    @Produces(MediaType.APPLICATION_JSON)
    public Note getNote(@PathParam("title") String title) {
        return noteController
                .getByTitle(title)
                .orElseThrow(NotFoundException::new);
    }
    @Path("/notes")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Note> getAll() {

        return this.noteController.getAll();
    }
}
